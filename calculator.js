
let operatorIsChosen = false;
let chosenOperator = '';
let firstNumber = 0;
let lastButtonWasEqual = false;

function number(insertedNumber) {
    let displayContent = document.querySelector('#display').textContent;
    if (displayContent.length < 10) {
        document.querySelector('#display').textContent += insertedNumber;
    }
    if (displayContent == "0" || lastButtonWasEqual) {
        document.querySelector('#display').textContent = insertedNumber;
    }
    if (operatorIsChosen) {
        document.querySelector('#display').textContent = insertedNumber;
        operatorIsChosen = false;
    }
    lastButtonWasEqual = false;
}

function ccc() {
    document.querySelector('#display').textContent = "0";
    operatorIsChosen = false;
    chosenOperator = '';
    lastButtonWasEqual = false;
}

function operation(operatorAsString) {
    operatorIsChosen = true;
    firstNumber = document.querySelector('#display').textContent;
    chosenOperator = operatorAsString;
    lastButtonWasEqual = false;
}

function equals() {
    if (!lastButtonWasEqual) {
        let result = 0;
        let secondNumber = document.querySelector('#display').textContent;
        switch (chosenOperator) {
            case "plus":
                result = (parseInt(firstNumber) + parseInt(secondNumber));
                break;
            case "minus":
                result = firstNumber - secondNumber;
                break;
            case "multiply":
                result = firstNumber * secondNumber;
                break;
            case "divide":
                result = firstNumber / secondNumber;
                break;
            default:
                break;
        }
        document.querySelector('#display').textContent = result.toString().slice(0, 14);
        operatorIsChosen = false;
        lastButtonWasEqual = true;
    }
}



